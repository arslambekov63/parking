public abstract class Transport {
    private String uniqueNumber;
    private int timeLeft;
    private boolean isOnPark;
    private String type;

    public String getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(String uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(int timeLeft) {
        this.timeLeft = timeLeft;
    }

    public void minusOneStep() {
        this.timeLeft = this.timeLeft - 1;
    }

    public Transport(String uniqueNumber, int timeLeft, boolean isOnPark, String type) {
        this.uniqueNumber = uniqueNumber;
        this.timeLeft = timeLeft;
        this.isOnPark = isOnPark;
        this.type= type;
    }

    public boolean isOnPark() {
        return isOnPark;
    }

    public void setOnPark(boolean onPark) {
        isOnPark = onPark;
    }
}
