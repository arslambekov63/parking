public class Parking {
    private int freePlacesCars;
    private int freePlacesTrucks;
    private int allPlaceCars;
    private int allPlaceTrucks;
    public Transport[] carArray;
    public Transport[] truckArray;
    public boolean[] isUniqueCodeBusy= new boolean[10000];

    public Parking(int allPlacesCars, int allPlacesTrucks, Transport[] cars, Transport[] trucks) {
        this.allPlaceCars = allPlacesCars;
        this.allPlaceTrucks = allPlacesTrucks;
        this.carArray = cars;
        this.truckArray = trucks;
    }

    public int getFreePlacesCars() {
        return freePlacesCars;
    }
    public int getFreePlacesTrucks() {
        return  freePlacesTrucks;
    }

    public void setFreePlaces(int freePlacesCars, int freePlacesTrucks) {
        this.freePlacesCars = freePlacesCars;
        this.freePlacesTrucks = freePlacesTrucks;
    }


    public void setCarArray(Car[] carArray) {
        this.carArray = carArray;
    }

    public void minusOneFreePlaceCars() {
        this.freePlacesCars = this.freePlacesCars - 1;
    }
    public void minusOneFreePlaceTruck() {
        this.freePlacesTrucks = this.freePlacesTrucks -1;
    }

    public boolean[] clearAllSpace(int parkingSpaceCars, int parkingSpaceTrucks, boolean[] isUniCodeFree) {
        this.setFreePlaces(parkingSpaceCars, parkingSpaceTrucks);
        for (int i = 0; i < 10000; i++) {
            isUniCodeFree[i] = false;
        }
        for (int i = 0; i < parkingSpaceCars; i++) {
            carArray[i].setTimeLeft(1);
            carArray[i].setOnPark(false);// чтоб в след ходе обнулилось
        }
        for (int i = 0; i<parkingSpaceTrucks; i++) {
            truckArray[i].setTimeLeft(1);
            truckArray[i].setOnPark(false);
        }
        return isUniCodeFree;
    }

    public void writeInfo(int minimalTimeLeftCars, int minimalTimeLeftTrucks) {
        System.out.println("Parking space for trucks: " + allPlaceTrucks);
        System.out.println("Parking space for cars: " + allPlaceCars);
        System.out.println("Free places for trucks: " + getFreePlacesTrucks());
        System.out.println("Free places for cars: " + getFreePlacesCars());
        int occupiedPlaceCars = allPlaceCars - freePlacesCars;
        int occupiedPlaceTrucks = allPlaceTrucks - freePlacesTrucks;
        System.out.println("Occupied places for cars: " + occupiedPlaceCars);
        System.out.println("Occupied places for trucks: " + occupiedPlaceTrucks);
        System.out.println("Next place for cars will be free in " + minimalTimeLeftCars + " step");
        System.out.println("Next place for trucks will be free in " + minimalTimeLeftTrucks + " step");
        for (int i=0; i<allPlaceCars; i++) {
            if (carArray[i].isOnPark()==true) {
                System.out.println("Place for cars number " + i + ". Type: " + carArray[i].getType() + " unique code: "
                        + carArray[i].getUniqueNumber() + " Time left: "+carArray[i].getTimeLeft() ) ;
            } else {
                System.out.println("Place for cars number " + i + ". Empty ");
            }
        }
        for (int i=0; i<allPlaceTrucks; i++) {
            if (truckArray[i].isOnPark()==true) {
            System.out.println("Place for trucks number " + i + ". Type: " + truckArray[i].getType() + " unique code: " +
                    truckArray[i].getUniqueNumber() + " Time left: "+truckArray[i].getTimeLeft());
        } else {
                System.out.println("Place for trucks number " + i + " . Empty ");
            }
        }
    }

    public void setFreePlacesCars(int freePlacesCars) {
        this.freePlacesCars = freePlacesCars;
    }

    public void setFreePlacesTrucks(int freePlacesTrucks) {
        this.freePlacesTrucks = freePlacesTrucks;
    }
}
