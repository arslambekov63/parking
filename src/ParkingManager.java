import java.util.Scanner;

public class ParkingManager {
    public static void main(String[] args) {
        System.out.println("How much places for cars will be in a parking?");
        Scanner scan = new Scanner(System.in);
        int parkingSpaceCars = scan.nextInt();
        System.out.println("How much places for trucks will be in a parking?");
        int parkingSpaceTrucks = scan.nextInt();
        Transport[] cars = new Transport[parkingSpaceCars];
        Transport[] trucks = new Transport[parkingSpaceTrucks];
        Parking parking = new Parking(parkingSpaceCars, parkingSpaceTrucks, cars, trucks);
        parking.setFreePlaces(parkingSpaceCars, parkingSpaceTrucks);
        boolean isEnough = false;
        boolean isSkipStep;
        for (int i = 0; i < 10000; i++) {
            parking.isUniqueCodeBusy[i] = false;
        }
        for (int i = 0; i < parkingSpaceCars; i++) {
            parking.carArray[i] = new Car("0", 0, false, "Car"); //
        }
        for (int i = 0; i < parkingSpaceTrucks; i++) {
            parking.truckArray[i] = new Truck("0", 0, false, "Truck");
        }
        while (isEnough == false) {
            parking.carArray = ParkingManager.minusTimeLeftTransport(parking.carArray, parkingSpaceCars); //Вычитаем один ход
            parking = ParkingManager.TransportLeftParking(parking.carArray, parkingSpaceCars, parking);//Машины с 0 ходами уезжают
            parking.truckArray = ParkingManager.minusTimeLeftTransport(parking.truckArray, parkingSpaceTrucks);//вычитаем ход из грузовиков
            parking = ParkingManager.TransportLeftParking(parking.truckArray, parkingSpaceTrucks, parking);//грузовики уезжают
            parking = generateAndEnterTrucks(parking, parkingSpaceTrucks, parkingSpaceCars);
            parking = generateAndEnterCars(parking, parkingSpaceCars, parkingSpaceTrucks);

            isSkipStep = false;
            while (isSkipStep == false) {
                System.out.println("choose next action:");
                System.out.println("1.Skip step 2.get info 3.clear parking");
                int action = scan.nextInt();
                switch (action) {
                    case 1:
                        isSkipStep = true;
                        break;
                    case 2:
                        parking.writeInfo(calculateMinimalTime(parking.carArray, parkingSpaceCars), calculateMinimalTime(parking.truckArray, parkingSpaceTrucks));
                        break;
                    case 3:
                        parking.isUniqueCodeBusy = parking.clearAllSpace(parkingSpaceCars, parkingSpaceTrucks, parking.isUniqueCodeBusy);
                        break;
                }
            }
        }

    }

    public static Transport[] minusTimeLeftTransport(Transport[] transports, int parkingSpaceCars) {
        for (int i = 0; i < parkingSpaceCars; i++) {
            if (transports[i].getTimeLeft() > 0) {
                int time = transports[i].getTimeLeft();
                transports[i].setTimeLeft(time - 1);
            }
        }
        return transports;
    }

    public static Parking TransportLeftParking(Transport[] transports, int parkingSpace, Parking parking) {
        for (int i = 0; i < parkingSpace; i++) {
            if (transports[i].getTimeLeft() == 0 & transports[i].isOnPark() == true) {
                System.out.println(transports[i].getType() + " " + transports[i].getUniqueNumber() + " left parking ");
                transports[i].setOnPark(false);
                transports[i].setUniqueNumber("0");//TODO boolean[]
                if (transports[i].getType().equals("Car")) {
                    int freeSpaceCar = parking.getFreePlacesCars() + 1;
                    parking.setFreePlacesCars(freeSpaceCar);
                    parking.carArray[i] = transports[i];
                } else {
                    int freeSpaceTruck = parking.getFreePlacesTrucks() + 1;
                    parking.setFreePlacesTrucks(freeSpaceTruck);
                    parking.truckArray[i] = transports[i];
                }
            }
        }
        return parking;
    }

    public static String generateUniqueCode(boolean[] isUniqueCodBusy) {
        boolean isFind = false;
        while (isFind == false) {
            int digit1 = (int) (Math.random() * 10);
            int digit2 = (int) (Math.random() * 10);
            int digit3 = (int) (Math.random() * 10);
            int digit4 = (int) (Math.random() * 10);
            int uniqueCode = digit4 + digit3 * 10 + digit2 * 100 + digit1 * 1000;
            char char1 = (char) digit1;
            String uniqueCodeString = "" + digit1 + digit2 + digit3 + digit4;
            if (isUniqueCodBusy[uniqueCode] == false) {
                isFind = true;
                return uniqueCodeString;
            }
        }
        return "0";
    }

    public static void writeFullParking(Transport[] transports, int parkingSpace) {
        System.out.println("Parking for " + transports[0].getType() + " is full" +
                ".  and next place will free in" + calculateMinimalTime(transports, parkingSpace) + "step");//TODO Calculate minimal time
    }

    public static int calculateMinimalTime(Transport[] transports, int parkingSpace) {
        int minimalTime = 11;
        for (int i = 0; i < parkingSpace; i++) {
            if (transports[i].isOnPark() == false) {
                minimalTime = 1;
                break;
            } else {
                if (transports[i].isOnPark() == true & transports[i].getTimeLeft() < minimalTime) {
                    minimalTime = transports[i].getTimeLeft();
                }
            }
        }
        if (minimalTime == 11) {
            minimalTime = 1;
        }
        return minimalTime;
    }

    public static int returnIntegerUniqueCode(String uniqueCode) {
        char[] arr = uniqueCode.toCharArray();
        int digit1 = arr[0] - '0';
        int digit2 = arr[1] - '0';
        int digit3 = arr[2] - '0';
        int digit4 = arr[3] - '0';

        int uniqueCodeInteger = digit4 + digit3 * 10 + digit2 * 100 + digit1 * 1000;

        return uniqueCodeInteger;
    }

    public static Parking generateAndEnterCars(Parking parking, int parkingSpaceCars, int parkingSpaceTrucks) {
        if (parking.getFreePlacesCars() != 0) {
            int carCount = (int) (Math.random() * ((parkingSpaceCars + parkingSpaceTrucks) / 3)); //количество новых машин в интервале от 0 до parkingSpaceCars /3
            while (parking.getFreePlacesCars() > 0 & carCount > 0) { //генерируем машины пока есть места и пока новые машины не закончилось
                int carTime = 1 + (int) (Math.random() * 10);
                String code = generateUniqueCode(parking.isUniqueCodeBusy);//генерируем четырехзначный код и проверяем на уникальность
                int intCode = returnIntegerUniqueCode(code);
                for (int i = 0; i < parkingSpaceCars; i++) {
                    if (parking.carArray[i].isOnPark() == false) {//паркуем машины на свободные места
                        parking.carArray[i].setOnPark(true);
                        parking.isUniqueCodeBusy[intCode] = true;
                        parking.carArray[i].setTimeLeft(carTime);
                        parking.carArray[i].setUniqueNumber(code);
                        carCount = carCount - 1;
                        parking.minusOneFreePlaceCars();
                        break;
                    }
                }
                if (parking.getFreePlacesCars() == 0) {
                    writeFullParking(parking.carArray, parkingSpaceCars);
                }
            }
        } else {
            writeFullParking(parking.carArray, parkingSpaceCars);
        }
        return parking;
    }

    public static Parking generateAndEnterTrucks(Parking parking, int parkingSpaceTruck, int parkingSpaceCars) {
        int truckCount = (int) (Math.random() * ((parkingSpaceTruck + parkingSpaceCars) / 3));
        if (parking.getFreePlacesTrucks() != 0) {
            while (parking.getFreePlacesTrucks() > 0 & truckCount > 0) {
                int truckTime = 1 + (int) (Math.random() * 10);
                String code = generateUniqueCode(parking.isUniqueCodeBusy);//генерируем четырехзначный код и проверяем на уникальность
                int intCode = returnIntegerUniqueCode(code);
                for (int i = 0; i < parkingSpaceTruck; i++) {
                    if (parking.truckArray[i].isOnPark() == false) {//паркуем машины на свободные места
                        parking.truckArray[i].setOnPark(true);
                        parking.isUniqueCodeBusy[intCode] = true;
                        parking.truckArray[i].setTimeLeft(truckTime);
                        parking.truckArray[i].setUniqueNumber(code);
                        truckCount = truckCount - 1;
                        parking.minusOneFreePlaceTruck();
                        break;
                    }
                }
                if (parking.getFreePlacesTrucks() == 0) {
                    writeFullParking(parking.truckArray, parkingSpaceTruck);
                }
            }
        }
        if (truckCount > 0) {
            for (int j = 0; j < truckCount; j++) {
                int truckTime = 1 + (int) (Math.random() * 10);
                String code = generateUniqueCode(parking.isUniqueCodeBusy);//генерируем четырехзначный код и проверяем на уникальность
                int intCode = returnIntegerUniqueCode(code);
                for (int i = 0; i < parkingSpaceCars - 1; i++) {
                    if (parking.carArray[i].isOnPark() == false & parking.carArray[i + 1].isOnPark() == false) {
                        parking.carArray[i].setOnPark(true);
                        parking.carArray[i + 1].setOnPark(true);
                        parking.carArray[i].setTimeLeft(truckTime);
                        parking.carArray[i + 1].setTimeLeft(truckTime);
                        parking.isUniqueCodeBusy[intCode] = true;
                        parking.carArray[i].setUniqueNumber(code);
                        parking.carArray[i + 1].setUniqueNumber(code);
                        parking.carArray[i].setType("Truck");
                        parking.carArray[i + 1].setType("Truck");
                        truckCount = truckCount - 1;
                        parking.minusOneFreePlaceCars();
                        parking.minusOneFreePlaceCars();
                        break;
                    }
                }
                if (parking.getFreePlacesCars() == 0) {
                    writeFullParking(parking.truckArray, parkingSpaceTruck);
                }

            }
        } else {
            writeFullParking(parking.truckArray, parkingSpaceTruck);
        }

        return parking;
    }
}